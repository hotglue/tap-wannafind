"""Stream type classes for tap-wannafind."""

from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Union

from singer_sdk import typing as th

from tap_wannafind.client import wannafindStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")

WannaFindArray = lambda x: th.ObjectType(th.Property("item", th.ArrayType(x)))

category_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("Description", th.StringType),
    th.Property("DescriptionBottom", th.StringType),
    th.Property("LanguageISO", th.StringType),
    th.Property("SeoDescripton", th.StringType),
    th.Property("SeoKeywords", th.StringType),
    th.Property("SeoTitle", th.StringType),
    th.Property("SeoLink", th.StringType),
    th.Property("SeoCanonical", th.StringType),
    th.Property("Title", th.StringType),
    th.Property("ParentId", th.IntegerType),
    th.Property("Status", th.BooleanType),
    th.Property("Sorting", th.IntegerType),
    th.Property("LanguageAccess", WannaFindArray(th.StringType)),
    th.Property("UserGroupAccessIds", WannaFindArray(th.IntegerType)),
    th.Property("ShowInMenu", WannaFindArray(th.IntegerType))
)

custom_data_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("CustomTypeId", th.IntegerType),
    th.Property("LanguageISO", th.StringType),
    th.Property("Title", th.StringType),
    th.Property("Sorting", th.IntegerType)
)

user_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("Username", th.StringType),
    th.Property("Password", th.StringType),
    th.Property("UserGroupId", th.IntegerType),
    th.Property("Type", th.StringType),
    th.Property("Company", th.StringType),
    th.Property("CustomData", WannaFindArray(custom_data_type)),
    th.Property("Cvr", th.StringType),
    th.Property("Ean", th.StringType),
    th.Property("Firstname", th.StringType),
    th.Property("Lastname", th.StringType),
    th.Property("Sex", th.IntegerType),
    th.Property("Address", th.StringType),
    th.Property("Address2", th.StringType),
    th.Property("Zip", th.StringType),
    th.Property("City", th.StringType),
    th.Property("Country", th.StringType),
    th.Property("CountryCode", th.IntegerType),
    th.Property("Currency", th.StringType),
    th.Property("Phone", th.StringType),
    th.Property("Mobile", th.StringType),
    th.Property("Fax", th.StringType),
    th.Property("Email", th.StringType),
    th.Property("Url", th.StringType),
    th.Property("DiscountGroupId", th.IntegerType),
    th.Property("Newsletter", th.BooleanType),
    th.Property("Referer", th.StringType),
    th.Property("BirthDate", th.StringType),
    th.Property("DateCreated", th.DateTimeType),
    th.Property("DateUpdated", th.DateTimeType),
    th.Property("Approved", th.BooleanType),
    th.Property("LanguageISO", th.StringType),
    th.Property("LanguageAccess", WannaFindArray(th.StringType)),
    th.Property("Description", th.StringType),
    th.Property("InterestFields", WannaFindArray(th.IntegerType)),
    th.Property("Number", th.StringType),
    th.Property("Site", th.IntegerType),
    th.Property("ShippingType", th.StringType),
    th.Property("ShippingFirstname", th.StringType),
    th.Property("ShippingLastname", th.StringType),
    th.Property("ShippingCompany", th.StringType),
    th.Property("ShippingCvr", th.StringType),
    th.Property("ShippingEan", th.StringType),
    th.Property("ShippingAddress", th.StringType),
    th.Property("ShippingAddress2", th.StringType),
    th.Property("ShippingZip", th.StringType),
    th.Property("ShippingCity", th.StringType),
    th.Property("ShippingCountry", th.StringType),
    th.Property("ShippingCountryCode", th.IntegerType),
    th.Property("ShippingState", th.StringType),
    th.Property("ShippingPhone", th.StringType),
    th.Property("ShippingMobile", th.StringType),
    th.Property("ShippingEmail", th.StringType),
    th.Property("ShippingReferenceNumber", th.StringType),
    th.Property("Consent", th.BooleanType),
    th.Property("ConsentDate", th.StringType),
)

delivery_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("Type", th.StringType),
    th.Property("ServiceType", th.StringType),
    th.Property("Vat", th.BooleanType),
    th.Property("FreeDeliveryActive", th.BooleanType),
    th.Property("FreeDeliveryPrice", th.NumberType),
    th.Property("OverLimitFeeActive", th.BooleanType),
    th.Property("OverLimitFixedFee", th.NumberType),
    th.Property("OverLimitPercentageFee", th.NumberType),
    th.Property("UserGroups", WannaFindArray(th.IntegerType)),
    th.Property("RegionMode", th.BooleanType),
    th.Property("ZipFrom", th.IntegerType),
    th.Property("ZipTo", th.IntegerType),
    th.Property("ZipGroups", WannaFindArray(th.IntegerType)),
    th.Property("DeliveryEstimate", th.BooleanType),
    th.Property("MultipleAddresses", th.BooleanType),
    th.Property("FixedDelivery", th.BooleanType),
    th.Property("Sorting", th.IntegerType),
    th.Property("Primary", th.BooleanType),
    th.Property("Title", th.StringType),
    th.Property("Text", th.StringType),
    th.Property("LanguageISO", th.StringType),
    th.Property("Price", th.NumberType)
)

product_delivery_time_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("LanguageISO", th.StringType),
    th.Property("Sorting", th.IntegerType),
    th.Property("TitleInStock", th.StringType),
    th.Property("TitleNoStock", th.StringType)
)

discount_group_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("Title", th.StringType),
    th.Property("Type", th.StringType),
    th.Property("Discount", th.NumberType)
)

product_unit_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("LanguageISO", th.StringType),
    th.Property("Title", th.StringType)
)

vat_group_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("Name", th.StringType),
    th.Property("VatPercentage", th.IntegerType),
    th.Property("Sorting", th.IntegerType)
)

product_custom_data_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("ProductCustomId", th.IntegerType),
    th.Property("ProductCustomIds", th.StringType),
    th.Property("ProductCustomTypeId", th.IntegerType),
    th.Property("LanguageISO", th.StringType),
    th.Property("Title", th.StringType),
    th.Property("Sorting", th.IntegerType),
)

product_picture_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("ProductId", th.IntegerType),
    th.Property("FileName", th.StringType),
    th.Property("Sorting", th.IntegerType)
)

product_category_sorting_type = th.ObjectType(
    th.Property("CategoryId", th.IntegerType),
    th.Property("Sorting", th.IntegerType)
)

product_additional_type_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("CategoryId", th.IntegerType),
    th.Property("LanguageISO", th.StringType),
    th.Property("Title", th.StringType)
)


order_customer_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("OrderId", th.IntegerType),
    th.Property("Firstname", th.StringType),
    th.Property("Lastname", th.StringType),
    th.Property("Company", th.StringType),
    th.Property("B2B", th.BooleanType),
    th.Property("Cvr", th.StringType),
    th.Property("Ean", th.StringType),
    th.Property("Address", th.StringType),
    th.Property("Address2", th.StringType),
    th.Property("Zip", th.StringType),
    th.Property("City", th.StringType),
    th.Property("Country", th.StringType),
    th.Property("CountryCode", th.StringType),
    th.Property("State", th.StringType),
    th.Property("Phone", th.StringType),
    th.Property("Mobile", th.StringType),
    th.Property("Email", th.StringType),
    th.Property("ShippingFirstname", th.StringType),
    th.Property("ShippingLastname", th.StringType),
    th.Property("ShippingCompany", th.StringType),
    th.Property("ShippingAddress", th.StringType),
    th.Property("ShippingAddress2", th.StringType),
    th.Property("ShippingZip", th.StringType),
    th.Property("ShippingCity", th.StringType),
    th.Property("ShippingCountry", th.StringType),
    th.Property("ShippingCountryCode", th.StringType),
    th.Property("ShippingState", th.StringType),
    th.Property("ShippingPhone", th.StringType),
    th.Property("ShippingMobile", th.StringType),
    th.Property("ShippingEmail", th.StringType)
)


product_extra_buy_relation_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("ProductId", th.IntegerType),
    th.Property("RelationProductId", th.IntegerType),
    th.Property("ExtraBuyCategoryId", th.IntegerType),
    th.Property("Sorting", th.IntegerType)
)

product_discount_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("ProductId", th.IntegerType),
    th.Property("ProductVariantId", th.IntegerType),
    th.Property("Amount", th.IntegerType),
    th.Property("Price", th.NumberType),
    th.Property("Discount", th.NumberType),
    th.Property("Currency", th.StringType),
    th.Property("DiscountType", th.StringType),
    th.Property("UserType", th.StringType),
    th.Property("UserId", th.IntegerType),
    th.Property("Date", th.BooleanType),
    th.Property("DateFrom", th.DateTimeType),
    th.Property("DateTo", th.DateTimeType),
    th.Property("Accumulate", th.BooleanType),
    th.Property("Site", th.IntegerType),
    th.Property("Language", th.StringType),
)

order_currency_type = th.ObjectType(
    th.Property("OrderId", th.IntegerType),
    th.Property("Id", th.IntegerType),
    th.Property("Iso", th.StringType),
    th.Property("Symbol", th.StringType),
    th.Property("SymbolPlace", th.StringType),
    th.Property("Currency", th.NumberType),
    th.Property("Decimal", th.StringType),
    th.Property("Point", th.StringType),
    th.Property("Round", th.IntegerType)
)

order_discount_code_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("OrderId", th.IntegerType),
    th.Property("DiscountId", th.IntegerType),
    th.Property("Title", th.StringType),
    th.Property("Type", th.StringType),
    th.Property("Value", th.NumberType),
    th.Property("Discount", th.NumberType),
    th.Property("Vat", th.BooleanType)
)

user_group_type = th.ObjectType(
    th.Property("Description", th.StringType),
    th.Property("OrderId", th.IntegerType),
    th.Property("PaymentMethodId", th.IntegerType),
    th.Property("Title", th.StringType),
    th.Property("Price", th.NumberType),
    th.Property("Vat", th.BooleanType)
)

order_payment_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("LanguageISO", th.StringType),
    th.Property("Title", th.StringType),
    th.Property("UserId", th.IntegerType),
    th.Property("Username", th.StringType),
    th.Property("UserEmail", th.StringType),
    th.Property("Rating", th.StringType),
    th.Property("Text", th.StringType),
    th.Property("DateCreated", th.DateTimeType)
)

order_transaction_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("OrderId", th.IntegerType),
    th.Property("PaymentId", th.IntegerType),
    th.Property("Status", th.IntegerType),
    th.Property("TransactionNumber", th.IntegerType),
    th.Property("TransactionNumberLong", th.StringType),
    th.Property("SubscriptionId", th.IntegerType),
    th.Property("SubscriptionIdLong", th.StringType),
    th.Property("Cardtype", th.StringType),
    th.Property("Amount", th.IntegerType),
    th.Property("AmountFull", th.NumberType),
    th.Property("Currency", th.StringType),
    th.Property("Errorcode", th.IntegerType),
    th.Property("Actioncode", th.IntegerType),
    th.Property("Date", th.DateType)
)

order_line_address_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("LineId", th.IntegerType),
    th.Property("Amount", th.IntegerType),
    th.Property("Firstname", th.StringType),
    th.Property("Lastname", th.StringType),
    th.Property("Company", th.StringType),
    th.Property("Address", th.StringType),
    th.Property("Zip", th.StringType),
    th.Property("City", th.StringType),
    th.Property("CountryIso", th.StringType),
    th.Property("Comment", th.StringType),
    th.Property("DeliveryDate", th.StringType),
    th.Property("DeliveryTime", th.StringType)
)


order_delivery_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("OrderId", th.IntegerType),
    th.Property("DeliveryMethodId", th.IntegerType),
    th.Property("Vat", th.BooleanType),
    th.Property("Title", th.StringType),
    th.Property("Price", th.NumberType),
    th.Property("BuyPrice", th.NumberType),
    th.Property("ServiceType", th.StringType),
    th.Property("DroppointId", th.IntegerType),
    th.Property("DroppointIdLong", th.StringType)
)


order_line_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("OrderId", th.IntegerType),
    th.Property("ProductId", th.IntegerType),
    th.Property("VariantId", th.IntegerType),
    th.Property("FileDownloadId", th.IntegerType),
    th.Property("Amount", th.IntegerType),
    th.Property("PacketTitle", th.StringType),
    th.Property("PacketId", th.IntegerType),
    th.Property("ProductTitle", th.StringType),
    th.Property("VariantTitle", th.StringType),
    th.Property("AdditionalTitle", th.StringType),
    th.Property("ItemNumber", th.StringType),
    th.Property("ItemNumberSupplier", th.StringType),
    th.Property("Discount", th.NumberType),
    th.Property("DiscountRounded", th.NumberType),
    th.Property("Price", th.NumberType),
    th.Property("PriceRounded", th.NumberType),
    th.Property("ServiceType", th.IntegerType),
    th.Property("BuyPrice", th.NumberType),
    th.Property("StockStatus", th.StringType),
    th.Property("Status", th.IntegerType),
    th.Property("TrackingCode", th.StringType),
    th.Property("Weight", th.NumberType),
    th.Property("LineAddresses", WannaFindArray(order_line_address_type)),
    th.Property("OfflineProduct", th.BooleanType),
    th.Property("VatRate", th.NumberType),
    th.Property("StockLocationId", th.IntegerType),
    th.Property("DeliveryId", th.IntegerType),
    th.Property("Unit", th.StringType),
    th.Property("ExtendedDataInternal", th.StringType),
    th.Property("ExtendedDataExternal", th.StringType)
)

product_tag_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("LanguageISO", th.StringType),
    th.Property("Title", th.StringType),
    th.Property("UserId", th.IntegerType),
    th.Property("Username", th.StringType),
    th.Property("UserEmail", th.StringType),
    th.Property("Rating", th.StringType),
    th.Property("Text", th.StringType),
    th.Property("DateCreated", th.DateTimeType)
)

order_packing_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("OrderId", th.IntegerType),
    th.Property("Text", th.StringType),
    th.Property("From", th.StringType)
)

product_variant_type = th.ObjectType(
    th.Property("Id", th.IntegerType),
    th.Property("ProductId", th.IntegerType),
    th.Property("Stock", th.IntegerType),
    th.Property("StockLow", th.IntegerType),
    th.Property("Price", th.NumberType),
    th.Property("BuyingPrice", th.NumberType),
    th.Property("ItemNumber", th.StringType),
    th.Property("ItemNumberSupplier", th.StringType),
    th.Property("Weight", th.NumberType),
    th.Property("DeliveryTime", product_delivery_time_type),
    th.Property("DeliveryTimeId", th.IntegerType),
    th.Property("Description", th.StringType),
    th.Property("DescriptionLong", th.StringType),
    th.Property("Status", th.BooleanType),
    th.Property("DisableOnEmpty", th.BooleanType),
    th.Property("Discount", th.NumberType),
    th.Property("DiscountType", th.StringType),
    th.Property("Ean", th.StringType),
    th.Property("PictureId", th.IntegerType),
    th.Property("PictureIds", WannaFindArray(th.IntegerType)),
    th.Property("Sorting", th.IntegerType),
    th.Property("VariantTypeValues", WannaFindArray(th.IntegerType)),
    th.Property("MinAmount", th.IntegerType),
    th.Property("Title", th.StringType),
    th.Property("Unit", product_unit_type)
)

class ProductsStream(wannafindStream):
    """Define product stream."""

    name = "products"
    primary_keys = ["Id", "DateUpdated"]
    soap_type = "Product_GetByUpdatedDate"
    replication_key = "DateUpdated"
    select_fields = "Product_SetFields"
    set_fields = ["Variants"]

    schema = th.PropertiesList(
        th.Property("Id", th.IntegerType),
        th.Property("CategoryId", th.IntegerType),
        th.Property("Category", category_type),
        th.Property("SecondaryCategoryIds", WannaFindArray(th.IntegerType)),
        th.Property("SecondaryCategories", WannaFindArray(category_type)),
        th.Property("ProducerId", th.IntegerType),
        th.Property("Producer", user_type),
        th.Property("Online", th.BooleanType),
        th.Property("Status", th.BooleanType),
        th.Property("DisableOnEmpty", th.BooleanType),
        th.Property("CallForPrice", th.BooleanType),
        th.Property("Stock", th.IntegerType),
        th.Property("ItemNumber", th.StringType),
        th.Property("ItemNumberSupplier", th.StringType),
        th.Property("RelationCode", th.StringType),
        th.Property("Url", th.StringType),
        th.Property("Weight", th.NumberType),
        th.Property("BuyingPrice", th.NumberType),
        th.Property("Price", th.NumberType),
        th.Property("Discount", th.NumberType),
        th.Property("DiscountType", th.StringType),
        th.Property("Delivery", delivery_type),
        th.Property("DeliveryId", th.IntegerType),
        th.Property("DeliveryTime", product_delivery_time_type),
        th.Property("DeliveryTimeId", th.IntegerType),
        th.Property("DiscountGroupId", th.IntegerType),
        th.Property("DiscountGroup", discount_group_type),
        th.Property("Ean", th.StringType),
        th.Property("FocusFrontpage", th.BooleanType),
        th.Property("FocusCart", th.BooleanType),
        th.Property("DateCreated", th.DateTimeType),
        th.Property("DateUpdated", th.DateTimeType),
        th.Property("RelatedProductIds", WannaFindArray(th.IntegerType)),
        th.Property("LanguageISO", th.StringType),
        th.Property("Title", th.StringType),
        th.Property("SeoTitle", th.StringType),
        th.Property("SeoDescription", th.StringType),
        th.Property("SeoKeywords", th.StringType),
        th.Property("SeoLink", th.StringType),
        th.Property("SeoCanonical", th.StringType),
        th.Property("Description", th.StringType),
        th.Property("DescriptionShort", th.StringType),
        th.Property("DescriptionLong", th.StringType),
        th.Property("Variants", WannaFindArray(product_variant_type)),
        th.Property("Tags", WannaFindArray(product_tag_type)),
        th.Property("Pictures", WannaFindArray(product_picture_type)),
        th.Property("CustomData", WannaFindArray(product_custom_data_type)),
        th.Property("Additionals", WannaFindArray(product_additional_type_type)),
        th.Property("ExtraBuyRelations", WannaFindArray(product_extra_buy_relation_type)),
        th.Property("UnitId", th.IntegerType),
        th.Property("Unit", product_unit_type),
        th.Property("UserAccess", WannaFindArray(user_type)),
        th.Property("UserAccessIds", WannaFindArray(th.IntegerType)),
        th.Property("UserGroupAccess", WannaFindArray(user_group_type)),
        th.Property("UserGroupAccessIds", WannaFindArray(th.IntegerType)),
        th.Property("Discounts", WannaFindArray(product_discount_type)),
        th.Property("MinAmount", th.IntegerType),
        th.Property("VariantTypes", th.StringType),
        th.Property("Sorting", th.IntegerType),
        th.Property("GuidelinePrice", th.NumberType),
        th.Property("ProductUrl", th.StringType),
        th.Property("LanguageAccess", WannaFindArray(th.StringType)),
        th.Property("VatGroupId", th.IntegerType),
        th.Property("VatGroup", vat_group_type),
        th.Property("AutoStock", th.StringType),
        th.Property("CategorySortings", WannaFindArray(product_category_sorting_type)),
        th.Property("OutOfStockBuy", th.StringType),
        th.Property("StockLow", th.IntegerType),
    ).to_dict()

class OrdersStream(wannafindStream):
    """Define orders stream."""

    name = "orders"
    primary_keys = ["Id", "DateUpdated"]
    soap_type = "Order_GetByDateUpdated"
    replication_key = "DateUpdated"
    select_fields = "Order_SetFields"
    set_fields = ["OrderLines"]

    schema = th.PropertiesList(
        th.Property("Id", th.IntegerType),
        th.Property("InvoiceNumber", th.IntegerType),
        th.Property("CurrencyId", th.IntegerType),
        th.Property("Currency", order_currency_type),
        th.Property("CustomerId", th.IntegerType),
        th.Property("Customer", order_customer_type),
        th.Property("UserId", th.IntegerType),
        th.Property("User", user_type),
        th.Property("Site", th.IntegerType),
        th.Property("LanguageISO", th.StringType),
        th.Property("Status", th.StringType),
        th.Property("PaymentId", th.IntegerType),
        th.Property("Payment", user_group_type),
        th.Property("Transactions", WannaFindArray(order_transaction_type)),
        th.Property("Vat", th.NumberType),
        th.Property("Total", th.NumberType),
        th.Property("OrderComment", th.StringType),
        th.Property("OrderCommentExternal", th.StringType),
        th.Property("CustomerComment", th.StringType),
        th.Property("DeliveryComment", th.StringType),
        th.Property("DeliveryTime", th.StringType),
        th.Property("TrackingCode", th.StringType),
        th.Property("DateDelivered", th.StringType),
        th.Property("DateDue", th.StringType),
        th.Property("DateSent", th.StringType),
        th.Property("DateUpdated", th.DateTimeType),
        th.Property("OrderLines", WannaFindArray(order_line_type)),
        th.Property("PackingId", th.IntegerType),
        th.Property("Packing", order_packing_type),
        th.Property("DeliveryId", th.IntegerType),
        th.Property("Delivery", order_delivery_type),
        th.Property("DiscountCodes", WannaFindArray(order_discount_code_type)),
        th.Property("ReferenceNumber", th.StringType),
        th.Property("Origin", th.StringType),
    ).to_dict()

class DiscountsStream(wannafindStream):
    """Define custom stream."""

    name = "discounts"
    primary_keys = ["Id"]
    soap_type = "Discount_GetAll"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("Id", th.IntegerType),
        th.Property("Code", th.StringType),
        th.Property("DateCreated", th.DateType),
        th.Property("DateExpire", th.DateType),
        th.Property("AmountSpent", th.IntegerType),
        th.Property("Limit", th.StringType),
        th.Property("ProductIds", WannaFindArray(th.IntegerType)),
        th.Property("Title", th.StringType),
        th.Property("Type", th.StringType),
        th.Property("UseCount", th.IntegerType),
        th.Property("Value", th.NumberType),
        th.Property("Vat", th.IntegerType)
    ).to_dict()

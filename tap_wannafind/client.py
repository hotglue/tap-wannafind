"""Custom client handling, including wannafindStream base class."""

from datetime import datetime, timedelta
from singer_sdk.plugin_base import PluginBase as TapBaseClass

from singer.schema import Schema
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk.streams import Stream
from zeep import Client, Settings, helpers


class wannafindStream(Stream):
    """Stream class for wannafind streams."""

    url = "https://api.hostedshop.io/service.wsdl"
    soap_client = Client(url)
    set_fields = []
    select_fields = False

    def __init__(
        self,
        tap: TapBaseClass,
        name: Optional[str] = None,
        schema: Optional[Union[Dict[str, Any], Schema]] = None,
        soap_type: Optional[str] = None,
    ) -> None:
        super().__init__(name=name, schema=schema, tap=tap)
        self.soap_client.service.Solution_Connect(
            self.config["username"], self.config["password"]
        )

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Return a generator of row-type dictionary objects.

        The optional `context` argument is used to identify a specific slice of the
        stream if partitioning is required for the stream. Most implementations do not
        require partitioning and should ignore the `context` argument.
        """

        gen_query_str = lambda x: ",".join(x)
        selected_properties = self.schema["properties"]
        query_str = gen_query_str(selected_properties.keys())

        if self.select_fields:
            getattr(self.soap_client.service, self.select_fields)(query_str)
        
        for field in self.set_fields:
            if field in selected_properties:
                field_properties = selected_properties[field]["properties"]
                if "item" in field_properties.keys():
                    field_properties = field_properties["item"]["items"]["properties"]
                query_str = gen_query_str(field_properties.keys())
                parent_name = self.soap_type.split('_')[0]
                child_name = field.rstrip('s')
                getattr(self.soap_client.service, f"{parent_name}_Set{child_name}Fields")(query_str)
        
        if self.replication_key:
            date = self.get_starting_timestamp(context)
            start = date.strftime("%Y-%m-%d %H:%M:%S")
            end = (datetime.now() + timedelta(1)).strftime("%Y-%m-%d")
            records = getattr(self.soap_client.service, self.soap_type)(Start=start, End=end)
        else:
            records = getattr(self.soap_client.service, self.soap_type)()

        # self.soap_client.wsdl.types.get_type(type).signature(schema=self.soap_client.wsdl.types)
        if records is not None:
            for row in records:
                yield helpers.serialize_object(row, dict)
